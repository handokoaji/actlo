<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /**
* Upload Controller
* Create by Rochmad Handoko Aji
* Seintica Production
* ajiilkom@ymail.com
*/
class Upload extends CI_Controller {
     function __construct() {
          parent::__construct();
          $this->load->helper(array('form', 'url','html','file'));
     }
 
     function index(){
          $this->session->sess_destroy();
          redirect('admin');
     }

     function img_upload(){
          if ($this->session->userdata('login') == TRUE) {
               $category = $this->input->post('category');
               $sub_data = array(
               'error' => '',
               'result' => ''
               );
          
          if ($this->input->post('go_upload')) {
               $config['upload_path'] = 'img/slider';
               $config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
               $config['max_size'] = '3000';
               //$config['max_width'] = '2048';
               //$config['max_height'] = '1536';

               $this->load->library('upload', $config);
               if (!$this->upload->do_upload()) {
                    $sub_data['error'] = $this->upload->display_errors();
               }else{
                    $sub_data['result'] = $this->upload->data();
               }
               foreach ($sub_data['result'] as $item => $value) {
                    $image_filename = $sub_data['result']['file_name'];
               }
               $this->db->insert('galery', array('image_name' => $image_filename, 'category' => $category));
          }
          $sub_data['list_gambar'] = $this->db->select('*')->from('galery')->get()->result();
          $data['title'] = "Upload image slider";
          //$sub_data['total'] = $this->db->select('*')->from('slider')->get()->result();
          $data['content'] = $this->load->view('img_upload', $sub_data, TRUE);
          $data['nav'] = $this->load->view('nav', NULL, TRUE);
          $this->load->view('dashboard', $data, FALSE);
          }else{
               redirect('admin');
          }
     }

     function img_delete($image_name){
          if ($this->session->userdata('login') == TRUE) {
               
               $this->db->where('image_name', $image_name)->delete('galery');
               $path_file = 'img/slider/'.$image_name;
               
               if (@unlink($path_file)) {
                        redirect('admin/upload/img_upload');
                   }elseif (file_exists($path_file)) {
                        redirect('admin/upload/img_upload');
                   }else{
                    redirect('admin/upload/img_upload');
                   }
          }else{
               redirect('admin');
          }
     }
}
?>