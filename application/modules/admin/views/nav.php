<nav class="navbar navbar-inverse editNav" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo(site_url()); ?>"><span class="glyphicon glyphicon-home"></span> actlo</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo(site_url('admin/dashboard')); ?>"><span class="glyphicon glyphicon-th-list"></span> Post list</a></li>
        <li><a href="<?php echo(site_url('admin/posting')); ?>"><span class="glyphicon glyphicon-plus"></span> Add Post</a></li>
        <li><a href="<?php echo(site_url('admin/upload/img_upload')); ?>"><span class="glyphicon glyphicon-picture"></span> Slider</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
        <li><a href="<?php echo(site_url('admin/logout')); ?>"><span class="glyphicon glyphicon-off"></span> logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>