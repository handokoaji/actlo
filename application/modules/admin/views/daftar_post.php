    <div class="row daftarPost">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-th-list"></span>
                <h3 class="panel-title">
                    Post list</h3>
                <span class="label label-info"><?php $a = $this->db->from('post')->count_all_results(); echo($a); ?></span>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                  <?php foreach ($daftar as $post) : ?>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <img src="http://placehold.it/80" class="img-circle img-responsive" alt="" /></div>
                            <div class="col-xs-10 col-md-11">
                                <div>
                                    <a href="#">
                                        <?php echo $post->judul; ?></a>
                                    <div class="mic-info">
                                        location : <?php echo $post->category; ?></div>
                                </div>
                                <div class="comment-text">
                                    
                                </div>
                                <div class="action">
                                    <a href="<?php echo(site_url("admin/ubah/{$post->id}")); ?>" class="btn-xs">
                                      <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="<?php echo(site_url("admin/hapus/{$post->id}")); ?>" class="btn-xs">
                                      <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    <a href="#" class="btn-xs">
                                      <span class="glyphicon glyphicon-fire"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php  endforeach; ?>
                </ul>
             </div>
        </div>
    </div>