<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo($title); ?></title>
    <link rel="shortcut icon" href="<?php echo(site_url()); ?>img/favicon.png" />
    <!-- Bootstrap -->
    <link href="<?php echo(site_url()); ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo(site_url()); ?>css/custom.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="row">
      <?php echo($nav); ?>
    </div>
    <div class="container dashboard">
      <?php echo($content); ?>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo(site_url()); ?>js/bootstrap.min.js"></script>
    <!--tinyMCE-->
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script>
        tinymce.init({selector:'textarea'
      });
        $('#popoverData').popover();
    </script>  
  </body>
</html>