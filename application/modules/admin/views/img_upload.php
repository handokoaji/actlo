<div class="row">
<?php
echo $error;
echo form_open_multipart('admin/upload/img_upload','role="form"'); 
?>   
      <div class="form-group">      
            <div class="col-xs-4">
            <input type="file" name="userfile" readonly><br>
            <select class="form-control" name="category">
              <option value="slider">Slider</option>
              <option value="sidebar_homepage">Sidebar Homepage</option>
              <option value="sidebar_about_actlo">Sidebar About Actlo</option>
              <option value="sidebar_about_developer">Sidebar About Developer</option>
              <option value="sidebar_download">Sidebar Download</option>
              <option value="sidebar_trainings">Sidebar Trainings</option>
              <option value="sidebar_contact">Sidebar Contact</option>
            </select>
          </div>
      </div>
      <div class="form-group">
        <div class="col-md-10">
            <span class="help-block">
              <a id="popoverData" class="btn" href="#" data-content="Slider images size 920x400px. Image size sidebar 250x250px (hanya 1 gambar). Pilih lokasi gambar untuk slider atau sidebar." rel="popover" data-placement="bottom" data-original-title="Ketentuan" data-trigger="hover">
              Hover this !</a>
            </span>
            <button type="submit" class="btn btn-primary" value="upload" name="go_upload">Upload</button>
          </div>
        </div>
<?php 
echo form_close();
//$i =1;
 ?></div><br>
    <div class="row daftarPost">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-th-list"></span>
                <h3 class="panel-title">
                    Image list</h3>
                <span class="label label-info"><?php $a = $this->db->from('galery')->count_all_results(); echo($a); ?></span>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                  <?php foreach ($list_gambar as $gambar) : ?>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
                                <img src="<?php echo(site_url()); ?>img/slider/<?php echo $gambar->image_name; ?>" class="img-thumbnail img-responsive" alt="" /></div>
                            <div class="col-xs-10 col-md-11">
                                <div>
                                    <a href="#">
                                        <?php echo $gambar->image_name; ?></a>
                                    <div class="mic-info">
                                        location : <?php echo $gambar->category; ?></div>
                                </div>
                                <div class="action">
                                    <a href="<?php echo(site_url("admin/upload/img_delete/{$gambar->image_name}")); ?>" class="btn-xs">
                                      <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                    <a href="#" class="btn-xs">
                                      <span class="glyphicon glyphicon-fire"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php  endforeach; ?>
                </ul>
             </div>
        </div>
    </div>