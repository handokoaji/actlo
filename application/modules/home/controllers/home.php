<?php if ( ! defined('BASEPATH')) exit('No direct script acces allowed'); 

/**
* Home Controller
* Create by Rochmad Handoko Aji
* Seintica Production
* 08 March 2014
* ajiilkom@ymail.com
*/

class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','text'));
		$this->load->library('pagination');
		$this->load->database();
	}

	function index(){
		$config = array();
		$config['base_url'] = site_url('home/page');
		//$config['total_rows'] = $this->db->count_all_results('post');
		$config['total_rows'] = $this->db->where('category', "home")->from('post')->count_all_results();
		$config['per_page'] = 2;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data1['links'] = $this->pagination->create_links();
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "home")->limit($config['per_page'], $page)->get()->result();
		$data1['pinned'] = $this->db->select('*')->from('post')->where('category', "pinned")->order_by('id', 'DESC')->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_homepage")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_homepage")->get()->result();
		$slider_on['gambar_slider'] = $this->db->select('*')->from('galery')->where('category', "slider")->get()->result();

		$data['title'] = "actlo";
		$data1['slider'] = $this->load->view('slider', $slider_on, TRUE);
		$data['content'] = $this->load->view('home_view', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}

	function page(){
		$config = array();
		$config['base_url'] = site_url('home/page');
		$config['total_rows'] = $this->db->where('category', "home")->from('post')->count_all_results();
		//$config['total_rows'] = $this->db->count_all_results('post');
		$config['per_page'] = 2;
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data1['links'] = $this->pagination->create_links();
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "home")->limit($config['per_page'], $page)->get()->result();
		$data['title'] = "actlo";

		$data1['pinned'] = $this->db->select('*')->from('post')->where('category', "pinned")->order_by('id', 'DESC')->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_homepage")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_homepage")->get()->result();
		$slider_on['gambar_slider'] = $this->db->select('*')->from('galery')->where('category', "slider")->get()->result();

		$data['title'] = "actlo";
		$data1['slider'] = $this->load->view('slider', $slider_on, TRUE);
		$data['content'] = $this->load->view('home_view', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}

	function actlo(){
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "actlo")->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_about_actlo")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_about_actlo")->get()->result();
		$data['title'] = "About actlo";
		//$data1['slider'] = $this->load->view('slider', NULL, TRUE);
		$data['content'] = $this->load->view('actlo_about', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}

	function developer(){
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "developer")->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_about_developer")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_about_developer")->get()->result();
		$data['title'] = "About actlo";
		//$data1['slider'] = $this->load->view('slider', NULL, TRUE);
		$data['content'] = $this->load->view('developer_about', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}

	function download(){
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "download")->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_download")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_download")->get()->result();
		$data['title'] = "About actlo";
		//$data1['slider'] = $this->load->view('slider', NULL, TRUE);
		$data['content'] = $this->load->view('download', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}

	function trainings(){
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "trainings")->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_trainings")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_trainings")->get()->result();
		$data['title'] = "About actlo";
		//$data1['slider'] = $this->load->view('slider', NULL, TRUE);
		$data['content'] = $this->load->view('trainings', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}

	function contact(){
		$data1['post'] = $this->db->select('*')->from('post')->where('category', "contact")->get()->result();
		$data1['sidebar'] = $this->db->select('*')->from('post')->where('category', "sidebar_contact")->order_by('id', 'DESC')->get()->result();
		$data1['gambar_sidebar'] = $this->db->select('*')->from('galery')->where('category', "sidebar_contact")->get()->result();
		$data['title'] = "About actlo";
		//$data1['slider'] = $this->load->view('slider', NULL, TRUE);
		$data['content'] = $this->load->view('contact', $data1, TRUE);
		$data['nav'] = $this->load->view('home_part/nav', NULL, TRUE);
		$data['footer'] = $this->load->view('home_part/footer', NULL, TRUE);
		$this->load->view('home_part/main_bg', $data, FALSE);
	}
}
?>