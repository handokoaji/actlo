<nav class="navbar navbar-default" role="navigation" id="navigasi">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <img src="img/logo.png" class="logo">
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navFont" id="bs-example-navbar-collapse-1">
      
     <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo(site_url()); ?>">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo(site_url('home/actlo')); ?>">actlo</a></li>
            <li><a href="<?php echo(site_url('home/developer')); ?>">Developer</a></li>
          </ul>
        </li>
        <li><a href="<?php echo(site_url('home/download')); ?>">Download</a></li>
            <li><a href="<?php echo(site_url('home/trainings')); ?>">Trainings</a></li>
            <li><a href="<?php echo(site_url('home/contact')); ?>">Contact</a></li>
      </ul>

      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>