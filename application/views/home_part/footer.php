 <div class="row" id="row footer">
      <div class="col-sm-6">
        <h4>About actlo</h4>
          <p class="small">
            <b>actlo</b> is a cross-platform productivity app which allows you to analyze your daily activity in the most efficient, effective and easy way. It works as a loss and waste analysis tools to help you identify area of improvements in activities that you spent your non value added activities.
          </p>
          <p>&copy 2014 actlo. Created by <a href="http://seintica.com" style="color:#fff;">Seintica Production</a>. All other copyrights and trademarks herein are the property of their respective owners.</p>
      </div>
      
      <div class="col-sm-6 col-md-3 footerKanan">
        <h4>Follow Us</h4>
          <ul class="fa-ul">
            <li><i class="fa-li fa fa-facebook"></i><a href="#">Facebook</a></li>
            <li><i class="fa-li fa fa-twitter"></i><a href="#">Twitter</a></li>
            <li><i class="fa-li fa fa-linkedin"></i><a href="#">Linkedin</a></li>
          </ul>
      </div>
      <div class="col-sm-6 col-md-3 footerKanan">
        <h4>Download</h4>
          <ul class="list-unstyled">
          <li><a href="#">for iPhone</a></li>
          <li><a href="#">for Android</a></li>
          <li><a href="#">for BlackBerry</a></li>
          </ul>
          <p style="font-size:10px;"><i>Page rendered in {elapsed_time} second</i></p>
      </div>
    </div>