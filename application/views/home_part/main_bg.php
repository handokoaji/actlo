<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo(site_url()); ?>" >
    <title><?php echo($title); ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo(site_url()); ?>css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Segoe UI">
    <link href="<?php echo(site_url()); ?>css/custom.css" rel="stylesheet">
    <!--font awesome-->
    <link rel="stylesheet" href="<?php echo(site_url()); ?>css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--parallax slider-->
    <link href="css/responsive-slider.css" rel="stylesheet" media="screen">
    <link rel="shortcut icon" href="<?php echo(site_url()); ?>img/favicon.png" />
  </head>
  <body>
    <!--nav-->
    <div class="container" id="background_batik">
    
    <div class="row">
      <?php echo($nav); ?>
      </div>
    
    
    <!--content-->
      <div class="container">
        <div class="row">
          <?php echo($content); ?>
        </div>
    </div>
    
    <!--footer-->
    
    
      <div class="row footer">
       <?php echo($footer); ?>
        
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo(site_url()); ?>js/bootstrap.min.js"></script>
    
    <script src="<?php echo(site_url()); ?>js/jquery.event.move.js"></script>
    <script src="<?php echo(site_url()); ?>js/responsive-slider.js"></script>
  </body>
</html>